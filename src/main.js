import Vue from "vue"
import App from "./App.vue"
import MessaePlugin from "@/utils/message.plugin"
import Loader from "@/components/common/loader/Loader"
import router from "./router"
import store from "./store"
import "./registerServiceWorker"
import "materialize-css/dist/js/materialize.min"
import {dateFilter} from "@/filters/filter.data"

Vue.config.productionTip = false;

/* Фильтры */
Vue.filter("date", dateFilter);

/* Подключение плагинов */
Vue.use(MessaePlugin);

/* Покдлючение компонентов  */
Vue.component("Loader", Loader);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
