export const inputString = () => import(`@/components/common/form/inputString`)

export const inputNumeric = () => import(`@/components/common/form/inputNumeric`)

export const inputDate = () => import(`@/components/common/form/inputDate`)

export const inputImage = () => import(`@/components/common/form/inputImage`)

