import Vue from 'vue';
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required, between, numeric } from 'vee-validate/dist/rules';

extend('required', {
  ...required,
  message: 'Поле {_field_} обязательно для заполнения!'
});

extend('max-length', {
  params: ['max'],
  validate(value, { max }) {
    let compareValue = 0;
    if (Number.isInteger(value)) {
      value = value.toString()
    }
    return value.length <= parseInt(max);
  },
  message: 'Поле {_field_} не должно содержать более {max} символов!',
});

extend('min', {
  params: ['min'],
  validate(value, { min }) {
    return parseInt(value) >= parseInt(min);
  },
  message: 'Поле {_field_} не должно быть меньше {min} символов!'
});

extend('numeric', {
  ...numeric,

  message: 'Поле {_field_} должно быть числом!'
});

extend('between', {
  ...between,
  params: ['min', 'max'],
  message: 'Поле {_field_} должно быть не менее {min} символов и неболее {max} символов!'
});

extend('minDate', {
  params: ['minDate'],
  validate(value, { minDate }) {

    const min = new Date(minDate);
    const currentValue = new Date(value);
    return min.getTime() <= currentValue.getTime() ;
  },
  message: 'Поле {_field_} должна быть не позже {minDate}!'
});

Vue.component('ValidationObserver', ValidationObserver);
Vue.component('ValidationProvider', ValidationProvider);
