import Vue from "vue"
import Vuex from "vuex"
import books from "./books"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        error: null,
        loading: false
    },
    mutations: {
        setError(state, error) {
            state.error = error;
        },
        clearError(state) {
            state.error = null;
        },
        setLoading(state, isLoading) {
            state.loading = isLoading
        }
    },
    getters: {
        error: state => state.error,
        loading: state => state.loading
    },
    actions: {},
    modules: {
        books
    }
});
