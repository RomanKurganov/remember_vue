const COLLECTION_BOOKS = "COLLECTION_BOOKS";
const initialState = {
    books: JSON.parse(localStorage.getItem(COLLECTION_BOOKS)) || []
}

export default {
    state: initialState,
    getters: {
        books: state => state.books
    },
    mutations: {
        /* добавление item вв хранилищие */
        addItem(state, book) {
            state.books = [...state.books, book];
        },
        /* обновление стора */
        updateBooks(state, books) {
            state.books = books
        },
        /* удаление определенного item в хранилище */
        removeItem(state, book) {
            const indexBook = state.books.map(item => item.id).indexOf(book.id);
            return state.books.splice(indexBook, 1);
        },
        /* редактирование хранилища */
        editItem(state, book) {

            state.books = state.books.map((item) => {
                return (item.id == book.id) ? book : item;
            });
        }
    },
    actions: {
        /* получение списка книг */
        async getBooks({getters, commit}) {
            commit('setLoading', true);
            const response = await JSON.parse(localStorage.getItem(COLLECTION_BOOKS));
            commit('setLoading', false)
            return response;
        },
        async setBooks({getters, commit}) {
            commit("updateBooks", getters.books);
            localStorage.setItem(COLLECTION_BOOKS, JSON.stringify(getters.books));
        },
        /* добавление книги */
        async addBook({commit, dispatch}, book) {
            commit("addItem", book);
            await dispatch('setBooks');
        },
        /* удаление книги */
        async removeBook({commit, dispatch}, book) {
            commit("removeItem", book);
            await dispatch('setBooks');
        },

        /* редактирование книги */
        async editBook({commit, dispatch}, book) {
            commit("editItem", book);
            await dispatch('setBooks');
        },
        /* получение книги по id */
        async getBookById({commit, getters}, bookId) {
            const response = getters.books.find(item => item.id == bookId);
            return response;
        }
    }
}
