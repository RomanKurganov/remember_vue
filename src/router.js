import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "listBooks",
      component: () => import("./views/ListBooks.vue")
    },
    {
      path: "/edit-book/:id?",
      name: "edit-book",
      component: () => import("./views/BookView.vue")
    }
  ]
});
